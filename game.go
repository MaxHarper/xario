package main

import (
	"fmt"

	"github.com/nsf/termbox-go"
)

var (
	pointsChan         = make(chan int)
	keyboardEventsChan = make(chan keyboardEvent)
)

// Start starts the game
func Start() {
	if err := termbox.Init(); err != nil {
		panic(err)
	}
	defer termbox.Close()

	go listenToKeyboard(keyboardEventsChan)

	for {
		select {
		case e:
			d := keyToDirection(e.key)
			fmt.Println(d)
		}
	}
}
