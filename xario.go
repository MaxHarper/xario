package main

import (
	"fmt"
	"os"

	"github.com/fatih/color"
	termbox "github.com/nsf/termbox-go"
)

type Player struct {
	x int
	y int
}

func New(prop1 int) Player {
	return Player{
		x: 2,
		y: 10,
	}
}

var curCol = 0
var curRune = 0
var backbuf []termbox.Cell
var bbw, bbh int

var runes = []rune{' ', '░', '▒', '▓', '█'}
var colors = []termbox.Attribute{
	termbox.ColorBlack,
	termbox.ColorRed,
	termbox.ColorGreen,
	termbox.ColorYellow,
	termbox.ColorBlue,
	termbox.ColorMagenta,
	termbox.ColorCyan,
	termbox.ColorWhite,
}

func move(p *Player, direction int) {
	switch direction {
	//Право
	case 1:
		termbox.SetCell(p.x, p.y, '1', termbox.ColorBlue, termbox.ColorBlue)
		p.x++
		termbox.SetCell(p.x, p.y, 'X', termbox.ColorRed, termbox.ColorBlue)
		termbox.Flush()
	//Лево
	case 2:
		termbox.SetCell(p.x, p.y, '1', termbox.ColorBlue, termbox.ColorBlue)
		p.x--
		termbox.SetCell(p.x, p.y, 'X', termbox.ColorRed, termbox.ColorBlue)
		termbox.Flush()

	//Вверх
	case 3:
		termbox.SetCell(p.x, p.y, '1', termbox.ColorBlue, termbox.ColorBlue)
		p.y--
		termbox.SetCell(p.x, p.y, 'X', termbox.ColorRed, termbox.ColorBlue)
		termbox.Flush()

	//Вниз
	case 4:
		termbox.SetCell(p.x, p.y, '1', termbox.ColorBlue, termbox.ColorBlue)
		p.y++
		termbox.SetCell(p.x, p.y, 'X', termbox.ColorRed, termbox.ColorBlue)
		termbox.Flush()
	}
}

func showLevel(str string) {
	var count = 0
	for _, element := range str {
		if count == 81 {
			count = 0
			fmt.Printf("\n")
		}
		switch element {
		case 75: // K
			color.Set(color.FgHiWhite)
			fmt.Printf("%c", 9619)
			color.Unset()
		case 48: // 0
			color.Set(color.FgBlue)
			fmt.Printf("%c", 9608)
			color.Unset()
		case 49: // 1
			color.Set(color.FgYellow)
			fmt.Printf("%c", 9608)
			color.Unset()
		case 50: //2
			color.Set(color.FgHiMagenta)
			color.Set(color.BgBlue)
			fmt.Printf("%c", 2591)
			color.Unset()
		case 78: // N
			color.Set(color.FgGreen)
			color.Set(color.BgBlue)
			fmt.Printf("%c", 9608)
			color.Unset()
		case 71: // G
			color.Set(color.FgHiBlue)
			color.Set(color.BgBlue)
			fmt.Printf("%c", 9608)
			color.Unset()
		case 88: // X
			color.Set(color.FgRed)
			color.Set(color.BgBlue)
			fmt.Printf("%c", 88)
			color.Unset()
		default:
			fmt.Printf("%c", 2591)
		}
		count++
		//fmt.Print(element)
	}

}

func main() {

	file, err := os.Open("map.txt")
	if err != nil {
		// здесь перехватывается ошибка
		fmt.Printf("Ошибка\n")
		return
	}
	defer file.Close()

	// получить размер файла
	stat, err := file.Stat()
	if err != nil {
		return
	}
	// чтение файла
	bs := make([]byte, stat.Size())
	_, err = file.Read(bs)
	if err != nil {
		return
	}

	str := string(bs)

	//fmt.Print("X"[0])

	err1 := termbox.Init()
	if err1 != nil {
		panic(err)
	}
	defer termbox.Close()

	termbox.SetInputMode(termbox.InputEsc)
	//fmt.Println(termbox.Size())
	//panic("debug")
	showLevel(str)
	termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)

	var player = new(Player)

loop:
	for {
		switch ev := termbox.PollEvent(); ev.Type {
		case termbox.EventKey:
			if ev.Key == termbox.KeyCtrlS {
				termbox.Sync()
			}
			if ev.Key == termbox.KeyCtrlD {
				break loop
			}
			if ev.Key == termbox.KeyArrowUp {
				move(player, 3)
			}
			if ev.Key == termbox.KeyArrowDown {
				move(player, 4)
			}
			if ev.Key == termbox.KeyArrowRight {
				move(player, 1)
			}
			if ev.Key == termbox.KeyArrowLeft {
				move(player, 2)
			}

		case termbox.EventResize:
			termbox.Clear(termbox.ColorDefault, termbox.ColorDefault)
			showLevel(str)

			termbox.Flush()
		case termbox.EventError:
			panic(ev.Err)
		}

		//showLevel(str)
		termbox.Flush()
	}
}
